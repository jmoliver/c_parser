
# C_parser README by Jake Oliver

This code is designed to operate in conjunction with another piece of C++ based code that uses a
main function. IT WILL NOT COMPILE/WORK ON ITS OWN. 
This code is specifically designed with C++ in mind, as python already has a pre-built package to
perform this task (argparse). 

## Usage

C_parser uses the getopt package to parse command line argument, and can be altered to match the
required input of the developers code. It will not run on its own. 

The code consists of three parts: a class called "Parser" which stores the info pulled from the
command line, a function called "PrintHelp" to print a list of the possible user options along
with whatever additional input each option may require, and a function called "ProcessArgs" which
pulls the information from the command line and stores it in the class. 

##### Incorporating C_parser into your code

In order to edit C_parser to work with your code, perform the following steps:

1. Edit the Parser class in parser.c to include only the variables you want to request from the 
   command line. Be sure to also update the class' setter/getter functions.
2. Edit the PrintHelp function to print information on your updated variables, and also list the 
   format of any additional input beyond the arguments themselves.
3. Edit the ProcessArgs function to correctly pull the inputted data and store it in the Parser 
   class following the convention set out in parser.c.

The finer details required for these edits are included in the parser.c file itself.

##### Including the updated parser.c file in your code

After you have performed the edits listed above, simply copy the parser.c file to your desired 
location and include it in the .c file where your main function is stored like so: 

```
#include "</path/to/file>/parser.c"
```

Replacing </path/to/file> with either the absolute path to parser.c or the relative path from
whichever directory your main function is stored under. 

Then, to allow parser.c to operate, your main function should look as follows:

```
int main(int argvc, char *argv[]){

    Parser options = ProcessArgs(argvc, argv);
```

This will create a Parser class called "options" at the beginning of your main function, which will
contain the variables you have previously specified in the parser.c file. You can access the stored
values using the setter/getter functions you have also previously defined in the parser.c file. 
These can then be incorporated into your code as variables, switches, etc.
#define xAH_cxx
#include <iostream>
#include <string>
#include <getopt.h>

using namespace std;

/* 
   ###########################################################################

     This code is designed to allow C++ programs to read command line input.
     Full instructions and explanation can be found in the C_parser README.

   ###########################################################################
*/

/*

  The Parser class stores the information from the command line so that it can be
  accessed by the main bulk of your code. It uses the standard C++ technique of
  storing the variables privately and using public setters/getters to access the 
  variables. You can replace any of the variables in this class with the variables
  you wish users to be able to input from the command line, but be sure to update
  the setter/getter functions accordingly.
  It is good practise to output a text document from your main code to make sure
  that you are using the options you intend, as a sanity check for when you
  find unusual errors. 

*/ 

class Parser {
 private: // Store your variables here.
  string inFile = "";
  string outFile = "";
  string channel = "";
  string selection = "";
  bool debug = false;
  bool noReco = false;
  bool noTruth = false;
  bool noSFs = false;
  bool noSampleWeights = false;
  bool noPRW = false;
  bool nominalOnly = true;
 public: // Mimic the structure to create setters/getters for any added variables
  void set_inFile(string inFile_){inFile = inFile_;} // Example setter function
  void set_outFile(string outFile_){outFile = outFile_;}
  void set_channel(string channel_){channel = channel_;}
  void set_selection(string selection_){selection = selection_;}
  void set_debug(bool debug_){debug = debug_;}
  void set_noReco(bool noReco_){noReco = noReco_;}
  void set_noTruth(bool noTruth_){noTruth = noTruth_;}
  void set_noSFs(bool noSFs_){noSFs = noSFs_;}
  void set_noSampleWeights(bool noSampleWeights_){noSampleWeights = noSampleWeights_;}
  void set_noPRW(bool noPRW_){noPRW = noPRW_;}
  void set_nominalOnly(bool nominalOnly_){nominalOnly = nominalOnly_;}
  string get_inFile() const {return inFile;} // Example getter function
  string get_outFile() const {return outFile;}
  string get_channel() const {return channel;}
  string get_selection() const {return selection;}    
  bool get_debug() const {return debug;}
  bool get_noReco() const {return noReco;}
  bool get_noTruth() const {return noTruth;}
  bool get_noSFs() const {return noSFs;}
  bool get_noSampleWeights() const {return noSampleWeights;}
  bool get_noPRW() const {return noPRW;}
  bool get_nominalOnly() const {return nominalOnly;}
};

/*

  The PrintHelp function prints the options available to the user to the console either
  on request (when the program is run with --help) or when the enter an invalid input.
  If your inputs require inputs, put the required inputs in <brackets>.

*/

void PrintHelp()
{
  cout <<
    "--inFile <path>:                  Set path to the input file\n"
    "--outFile <path>:                 Set path to the output file directory\n"
    "--channel <mu, e>:                Set lepton channel to run over (case sensitive)\n"
    "--selection <SR, SR20, SR_noMET>: Set selection from options in selections.c\n"
    "--debug:                          Enable debug mode\n"
    "--noReco:                         Enable running without reco analysis\n"
    "--noTruth:                        Enable running without truth analysis\n"
    "--noSFs:                          Enable running without scale factors\n"
    "--noSampleWeights:                Enable running without sample weights\n"
    "--noPRW:                          Enable running without pileup reweighting\n"
    "--nominalOnly:                    Enable running with only nominal events\n";
  exit(1); //Note that the code will not run if the PrintHelp function is called.
}

/*

  The ProcessArgs returns a class of the type we previously defined, pulls the information
  from the command line to be stored in the class (which can then be accessed by the remainder
  of your code. The Parser can identifies the information you store with the short_opts and
  long_opts variables. Short_opts defines a character for each input, so that it can be used
  in the switch block later in the function. Long_opts links each character with the input
  argument so it can be recognised. Note that if a command line option requires input, it must
  have certain settings - a colon following it in short_ops, and "required_argument" following
  it in long_opts.

*/

Parser ProcessArgs(int argc, char** argv)
{
  
  Parser options;
  
  const char* const short_opts = "i:o:c:s:drtfwpnh";
  const option long_opts[] = {
    {"inFile", required_argument, nullptr, 'i'},
    {"outFile", required_argument, nullptr, 'o'},
    {"channel", required_argument, nullptr, 'c'},
    {"selection", required_argument, nullptr, 's'},
    {"debug", no_argument, nullptr, 'd'},
    {"noReco", no_argument, nullptr, 'r'},
    {"noTruth", no_argument, nullptr, 't'},
    {"noSFs", no_argument, nullptr, 'f'},
    {"noSampleWeights", no_argument, nullptr, 'w'},
    {"noPRW", no_argument, nullptr, 'p'},
    {"nominalOnly", no_argument, nullptr, 'n'},
    {"help", no_argument, nullptr, 'h'}
  };
  
  // This loop runs over all command line input and breaks after it has all been identified.
  // If the input is incorrect/unrecognised, the default argument calls PrintHelp and ends the loop.

  while(true)
    {
      const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
      
      if (-1 == opt) // 
	break;
      
      switch (opt)
        {
	  
        case 'i':
	  {
            string str(optarg);
            options.set_inFile(str);
            cout << "Input file path: " << options.get_inFile() << endl;
	  }
	  break;
	  
        case 'o':
	  {
            string str(optarg);
            options.set_outFile(str);
            cout << "Output directory path: " << options.get_outFile() << "/output" << endl;
	  }
	  break;

        case 'c':  // It is better to exit here if input is incorrect than in the main code body.
	  {
            string str(optarg);
            if(str.find("mu") != string::npos){
	      options.set_channel("mumu");
            } else if (str.find("e") != string::npos){
	      options.set_channel("ee");
            } else {
	      cout << "Invalid lepton channel. Exiting..." << endl;
	      exit(1);
            }
            
            cout << "Lepton channel is: " << options.get_channel() << endl;
	  }
	  break;
	  
        case 's':
	  {
            string str(optarg);
            if(str.find("SRselection") != string::npos){
	      options.set_selection("SRselection");
            } else if(str.find("SR20selection") != string::npos){
	      options.set_selection("SR20selection");
            } else if(str.find("SR_noMET_Selection") != string::npos){
	      options.set_selection("SR_noMET_Selection");
            } else {
	      cout << "Invalid selection. Exiting..." << endl;
	      exit(1);
            }
	    
            cout << "Selection is: " << options.get_selection() << endl;
	  }
	  break;
	  
	  // Bools do not require specific input - their presence is enough to know the user's intent. 
        case 'd': 
	  options.set_debug(true);
	  cout << "Debug mode enabled" << endl;
	  break;
	  
        case 'r':
	  options.set_noReco(true);
	  cout << "noReco mode enabled" << endl;
	  break;
	  
        case 't':
	  options.set_noTruth(true);
	  cout << "noTruth mode enabled" << endl;
	  break;
	  
        case 'f':
	  options.set_noSFs(true);
	  cout << "noSFs mode enabled" << endl;
	  break;
	  
        case 'w':
	  options.set_noSampleWeights(true);
	  cout << "noSampleWeights mode enabled" << endl;
	  break;
	  
        case 'p':
	  options.set_noPRW(true);
	  cout << "noPRW mode enabled" << endl;
	  break;
	  
        case 'n':
	  options.set_nominalOnly(true);
	  cout << "nominalOnly mode enabled" << endl;
	  break;
	  
	  // If help or invalid input is called, PrintHelp is called and the function ends. 

        case 'h':
        case '?':
        default:
	  PrintHelp();
	  break;
        }
      
    }
    
  return options;
  // The function returns a class that has stored the options pulled from the command line. 
}
